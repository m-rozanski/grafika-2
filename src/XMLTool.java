import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XMLTool {
    private Document doc;
    private Element root;
    private String name = "";

    public void initNew(String Name) throws ParserConfigurationException {
        DocumentBuilderFactory factory;
        DocumentBuilder builder;
        name = Name;
        factory = DocumentBuilderFactory.newInstance();
        builder = factory.newDocumentBuilder();
        doc = builder.newDocument();
        root = doc.createElement("selections");
        doc.appendChild(root);
        root.setAttribute("id", name);
    }

    public void add(Shape shape) {
        root.appendChild(createSelection(shape));
    }

    private Node createSelection(Shape shape) {
        Element selection = doc.createElement("selection");

        selection.appendChild(createValue("mode", shape.mode));
        selection.appendChild(createColor(shape.color));
        selection.appendChild(createPoints(shape.points));

        return selection;
    }

    private Node createColor(Color color) {
        Element c = doc.createElement("color");
        c.appendChild(createValue("red", String.valueOf(color.getRed())));
        c.appendChild(createValue("green", String.valueOf(color.getGreen())));
        c.appendChild(createValue("blue", String.valueOf(color.getBlue())));
        return c;
    }

    private Node createPoints(ArrayList<Point> points) {
        Element p = doc.createElement("points");
        for (Point point : points) {
            p.appendChild(createPoint(point));
        }
        return p;
    }

    private Node createPoint(Point point) {
        Element p = doc.createElement("point");
        p.appendChild(createValue("x", String.valueOf(point.x)));
        p.appendChild(createValue("y", String.valueOf(point.y)));
        return p;
    }

    private Node createValue(String tag, String value) {
        Element val = doc.createElement(tag);
        val.appendChild(doc.createTextNode(value));
        return val;
    }

    public void save(String path) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        DOMSource source = new DOMSource(doc);
        File file = new File(path + "\\" + name + ".xml");
        StreamResult f = new StreamResult(file);
        transformer.transform(source, f);
    }

    public boolean isCompatible(File file, String imageName) throws ParserConfigurationException, IOException, SAXException{
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        doc = builder.parse(file);
        doc.getDocumentElement().normalize();

        Node selectionRoot = doc.getElementsByTagName("selections").item(0);
        if (selectionRoot.getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) selectionRoot;
            String xmlName = element.getAttribute("id");
            return xmlName.equals(imageName);
        }

        return false;
    }

    public ArrayList<Shape> read(File file) {
        NodeList selectionList = doc.getElementsByTagName("selection");
        ArrayList<Shape> shapes = new ArrayList<>();

        for (int i = 0; i < selectionList.getLength(); i++) {
            Node selection = selectionList.item(i);
            if (selection.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) selection;
                Node mode = element.getElementsByTagName("mode").item(0);
                String mode_str = mode.getTextContent();

                Color color = getColor(element.getElementsByTagName("color").item(0));
                ArrayList<Point> points = getPoints(element.getElementsByTagName("points").item(0));

                shapes.add(new Shape(points, mode_str, null, color));
            }
        }

        return shapes;
    }

    private ArrayList<Point> getPoints(Node node) {
        ArrayList<Point> points = new ArrayList<>();
        NodeList points_xml;

        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            points_xml = element.getElementsByTagName("point");

            for (int i = 0; i < points_xml.getLength(); i++) {
                points.add(getPoint(points_xml.item(i)));
            }
        }

        return points;
    }

    private Point getPoint(Node node){
        Point point = null;

        if(node.getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) node;
            String x = element.getElementsByTagName("x").item(0).getTextContent();
            String y = element.getElementsByTagName("y").item(0).getTextContent();
            point = new Point(Integer.parseInt(x), Integer.parseInt(y));
        }

        return point;
    }

    private Color getColor(Node node) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element element = (Element) node;
            String red = element.getElementsByTagName("red").item(0).getTextContent();
            String green = element.getElementsByTagName("green").item(0).getTextContent();
            String blue = element.getElementsByTagName("blue").item(0).getTextContent();
            return new Color(Integer.parseInt(red), Integer.parseInt(green), Integer.parseInt(blue));
        }
        return null;
    }

}
