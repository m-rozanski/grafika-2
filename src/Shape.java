import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Shape {
    ArrayList<Point> points;
    String mode;
    BufferedImage image;
    Color color;

    Shape(ArrayList<Point> Points, String Mode, BufferedImage Image, Color Color) {
        points = Points;
        mode = Mode;
        image = Image;
        color = Color;
    }

    @Override
    public String toString() {
        String points_str = "Points: ";
        for (int i = 0; i < points.size(); i++) {
            points_str += points.get(i).toString();
            if (i != points.size() - 1)
                points_str += ", ";
        }
        String color_str = "RGB=(" + color.getRed() + "," + color.getGreen() + "," + color.getBlue() + ")";
        return mode + " " + color_str + " " + points_str;
    }
}
