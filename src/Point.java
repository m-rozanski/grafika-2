public class Point {
    int x = 0;
    int y = 0;

    Point(int X, int Y) {
        x = X;
        y = Y;
    }

    @Override
    public String toString(){
        return "(" + x + "," + y +")";
    }
}