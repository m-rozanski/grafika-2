import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;

class ToolBar extends JToolBar implements ActionListener {
    private ArrayList<String> names = new ArrayList<String>(Arrays.asList("open", "load XML", "save", "rectangle", "ellipse", "polygon", "colorPicker"));
    private JButton[] buttons = new JButton[names.size()];
    private String iconPath = "C:\\Users\\Robocze\\IdeaProjects\\Grafika 2\\src\\assets\\";
    private String iconFormat = ".png";
    private int size = 30;
    private ToolBarClickListener listener;

    public interface ToolBarClickListener {
        void openClicked();
        void loadClicked();
        void saveClicked();
        void rectangleClicked();
        void ellipseClicked();
        void polygonClicked();
        void colorPickerClicked();
    }

    ToolBar() {
        super();
        this.setRollover(false);

        for (int i = 0; i < names.size(); i++) {
            ImageIcon icon = new ImageIcon(iconPath + names.get(i) + iconFormat);
            Image img = icon.getImage().getScaledInstance(size, size, Image.SCALE_SMOOTH);
            buttons[i] = new JButton(new ImageIcon(img));
            buttons[i].setToolTipText(names.get(i));
            buttons[i].setBorder(BorderFactory.createEmptyBorder());
            buttons[i].setContentAreaFilled(false);
            buttons[i].addActionListener(this);
            buttons[i].setActionCommand(names.get(i));
        }

        this.addSeparator();
        this.add(buttons[0]);
        this.addSeparator();
        this.add(buttons[1]);
        this.addSeparator();
        this.add(buttons[2]);
        this.addSeparator(new Dimension(size, size));
        this.add(buttons[3]);
        this.addSeparator();
        this.add(buttons[4]);
        this.addSeparator();
        this.add(buttons[5]);
        this.addSeparator(new Dimension(size, size));
        this.add(buttons[6]);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int id = names.indexOf(e.getActionCommand());

        switch (id) {
            case 0:
                listener.openClicked();
                break;
            case 1:
                listener.loadClicked();
                break;
            case 2:
                listener.saveClicked();
                break;
            case 3:
                listener.rectangleClicked();
                break;
            case 4:
                listener.ellipseClicked();
                break;
            case 5:
                listener.polygonClicked();
                break;
            case 6:
                listener.colorPickerClicked();
                break;
            default:
                writeln("Toolbar action " + id + " not implemented");
                break;
        }
    }

    private void writeln(String str) {
        System.out.println(str);
    }

    public void setListener(ToolBarClickListener Listener){
        listener = Listener;
    }
}