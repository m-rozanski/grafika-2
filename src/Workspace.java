import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

class Workspace extends JPanel implements ActionListener, MouseListener, MouseMotionListener {
    private Dimension size = getSize();
    private Graphics2D g2d;
    private BufferedImage image = null;

    private final String MODE_RECTANGLE = "MODE_RECTANGLE";
    private final String MODE_ELLIPSE = "MODE_ELLIPSE";
    private final String MODE_POLYGON = "MODE_POLYGON";
    private String MODE_ACTUAL = MODE_RECTANGLE;
    private Color color = new Color(0, 0, 0);

    private ArrayList<Point> points = new ArrayList<>();
    private ArrayList<Shape> shapes = new ArrayList<>();

    Workspace() {
        super();
        addMouseListener(this);
        addMouseMotionListener(this);
    }

    // -------------------------------------------------------------------

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        size = getSize();
        g2d = (Graphics2D) g;

        if (image != null) {
            g2d.drawImage(image, 0, 0, null);

            for (Shape shape : shapes) {
                g2d.setColor(shape.color);
                drawShape(shape);
            }

            g2d.setColor(color);
            drawActualShape();
        }
    }

    private void drawShape(Shape shape) {
        if (shape.mode.equals(MODE_RECTANGLE)) drawRectangle(shape);
        if (shape.mode.equals(MODE_ELLIPSE)) drawEllipse(shape);
        if (shape.mode.equals(MODE_POLYGON)) drawPolygon(shape);
    }

    private void drawRectangle(Shape shape) {
        if (shape.points.size() == 2) {
            // change the points values
            ArrayList<Point> newPoints = changePointsForRectangleAndEllipse(shape.points);
            int width = newPoints.get(1).x - newPoints.get(0).x;
            int height = newPoints.get(1).y - newPoints.get(0).y;
            g2d.drawRect(newPoints.get(0).x, newPoints.get(0).y, width, height);
        }
    }

    private void drawEllipse(Shape shape) {
        if (shape.points.size() == 2) {
            // change the points values
            ArrayList<Point> newPoints = changePointsForRectangleAndEllipse(shape.points);
            int width = newPoints.get(1).x - newPoints.get(0).x;
            int height = newPoints.get(1).y - newPoints.get(0).y;
            g2d.drawOval(newPoints.get(0).x, newPoints.get(0).y, width, height);
        }
    }

    private void drawPolygon(Shape shape) {
        if (shape.points.size() < 3) {
            for (Point p : shape.points) {
                g2d.fillOval(p.x - 3, p.y - 3, 6, 6);
            }
        } else {
            Polygon polygon = new Polygon();
            for (Point p : shape.points) {
                polygon.addPoint(p.x, p.y);
            }
            g2d.draw(polygon);
        }
    }

    private void drawActualShape() {
        if (MODE_ACTUAL.equals(MODE_RECTANGLE)) drawActualRectangle();
        if (MODE_ACTUAL.equals(MODE_ELLIPSE)) drawActualEllipse();
        if (MODE_ACTUAL.equals(MODE_POLYGON)) drawActualPolygon();
    }

    private void drawActualRectangle() {
        if (points.size() == 2) {
            // change the points values
            ArrayList<Point> newPoints = changePointsForRectangleAndEllipse(points);
            int width = newPoints.get(1).x - newPoints.get(0).x;
            int height = newPoints.get(1).y - newPoints.get(0).y;
            g2d.drawRect(newPoints.get(0).x, newPoints.get(0).y, width, height);
        }
    }

    private void drawActualEllipse() {
        if (points.size() == 2) {
            // change the points values
            ArrayList<Point> newPoints = changePointsForRectangleAndEllipse(points);
            int width = newPoints.get(1).x - newPoints.get(0).x;
            int height = newPoints.get(1).y - newPoints.get(0).y;
            g2d.drawOval(newPoints.get(0).x, newPoints.get(0).y, width, height);
        }
    }

    private void drawActualPolygon() {
        if (points.size() < 3) {
            for (Point p : points) {
                g2d.fillOval(p.x - 3, p.y - 3, 6, 6);
            }
        } else {
            Polygon polygon = new Polygon();
            for (Point p : points) {
                polygon.addPoint(p.x, p.y);
            }
            g2d.draw(polygon);
        }

    }

    // -------------------------------------------------------------------

    public void setModeRectangle() {
        points.clear();
        MODE_ACTUAL = MODE_RECTANGLE;
        repaint();
    }

    public void setModeEllipse() {
        points.clear();
        MODE_ACTUAL = MODE_ELLIPSE;
        repaint();
    }

    public void setModePolygon() {
        points.clear();
        MODE_ACTUAL = MODE_POLYGON;
        repaint();
    }

    public void setColor(Color newColor) {
        if (newColor != null)
            color = newColor;
    }

    public void loadImage(BufferedImage img) {
        image = img;
        repaint();
    }

    public Boolean isImageLoaded() {
        return image != null;
    }

    public void loadImageToShape(Shape shape) {
        BufferedImage result = null;

        switch (shape.mode) {
            case MODE_RECTANGLE:
                result = cropImageToRectangle(shape.points);
                break;
            case MODE_ELLIPSE:
                result = cropImageToEllipse(shape.points);
                break;
            case MODE_POLYGON:
                result = cropImageToPolygon(shape.points);
                break;
        }

        shape.image = result;
    }

    private ArrayList<Point> changePointsForRectangleAndEllipse(ArrayList<Point> oldPoints) {
        ArrayList<Point> newPoints = new ArrayList<>();

        int p0x = oldPoints.get(0).x, p0y = oldPoints.get(0).y, p1x = oldPoints.get(1).x, p1y = oldPoints.get(1).y;
        int new0x = oldPoints.get(0).x;
        int new0y = oldPoints.get(0).y;
        int new1x = oldPoints.get(1).x;
        int new1y = oldPoints.get(1).y;

        if (p1x < p0x && p1y < p0y) {
            new0x = p1x;
            new0y = p1y;
            new1x = p0x;
            new1y = p0y;
        } else if (p1x < p0x) {
            new0x = p1x;
            new0y = p0y;
            new1x = p0x;
            new1y = p1y;
        } else if (p1y < p0y) {
            new0x = p0x;
            new0y = p1y;
            new1x = p1x;
            new1y = p0y;
        }

        Point point0 = new Point(new0x, new0y);
        Point point1 = new Point(new1x, new1y);
        newPoints.add(point0);
        newPoints.add(point1);
        return newPoints;
    }

    public void clear() {
        points.clear();
        shapes.clear();
        image = null;
    }

    // -------------------------------------------------------------------
    private BufferedImage cropImageToEllipse(ArrayList<Point> points) {
        BufferedImage newImg = null;

        ArrayList<Point> newPoints = changePointsForRectangleAndEllipse(points);
        int width = newPoints.get(1).x - newPoints.get(0).x;
        int height = newPoints.get(1).y - newPoints.get(0).y;

        BufferedImage croppedImage = image.getSubimage(newPoints.get(0).x, newPoints.get(0).y, width, height);
        newImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = newImg.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fill(new Ellipse2D.Double(0, 0, width, height));
        g2d.dispose();
        int transparency = new Color(0, 0, 0, 0).getTransparency();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (newImg.getRGB(x, y) == Color.WHITE.getRGB()) {
                    newImg.setRGB(x, y, croppedImage.getRGB(x, y));
                } else {
                    newImg.setRGB(x, y, transparency);
                }
            }
        }
        return newImg;
    }

    private BufferedImage cropImageToRectangle(ArrayList<Point> points) {
        BufferedImage newImg = null;

        ArrayList<Point> newPoints = changePointsForRectangleAndEllipse(points);
        int width = newPoints.get(1).x - newPoints.get(0).x;
        int height = newPoints.get(1).y - newPoints.get(0).y;

        newImg = image.getSubimage(newPoints.get(0).x, newPoints.get(0).y, width, height);
        return newImg;
    }

    private BufferedImage cropImageToPolygon(ArrayList<Point> points) {
        BufferedImage newImg = null;

        Polygon polygon = new Polygon();
        for (Point p : points) {
            polygon.addPoint(p.x, p.y);
        }

        Rectangle2D rectangle = polygon.getBounds2D();

        int width = (int) rectangle.getWidth();
        int height = (int) rectangle.getHeight();

        BufferedImage croppedImage = image.getSubimage((int) rectangle.getX(), (int) rectangle.getY(), width, height);
        newImg = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Polygon newPolygon = new Polygon();
        for (Point p : points) {
            newPolygon.addPoint(p.x - (int) rectangle.getX(), p.y - (int) rectangle.getY());
        }

        Graphics2D g2d = newImg.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fill(newPolygon);
        g2d.dispose();
        int transparency = new Color(0, 0, 0, 0).getTransparency();
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (newImg.getRGB(x, y) == Color.WHITE.getRGB()) {
                    newImg.setRGB(x, y, croppedImage.getRGB(x, y));
                } else {
                    newImg.setRGB(x, y, transparency);
                }
            }
        }
        return newImg;
    }

    public Shape getShapeFromImage() {
        BufferedImage result = null;

        if (points.size() > 2)
            result = cropImageToPolygon(points);
        else if (points.size() == 2) {
            if (MODE_ACTUAL.equals(MODE_RECTANGLE))
                result = cropImageToRectangle(points);
            else if (MODE_ACTUAL.equals(MODE_ELLIPSE))
                result = cropImageToEllipse(points);
        }

        return new Shape((ArrayList<Point>) points.clone(), MODE_ACTUAL, result, color);
    }

    public void addShape(Shape shape) {
        shapes.add(shape);
        repaint();
    }

    public void deleteShape(int index) {
        shapes.remove(index);
        repaint();
    }

    // -------------------------------------------------------------------

    private int actualMouse = 0;

    @Override
    public void mouseClicked(MouseEvent e) {
        actualMouse = e.getButton();

        if (actualMouse == MouseEvent.BUTTON1) {
            if (MODE_ACTUAL.equals(MODE_POLYGON)) {
                points.add(new Point(e.getX(), e.getY()));
                repaint();
            }
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            points.clear();
            repaint();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        actualMouse = e.getButton();

        if (actualMouse == MouseEvent.BUTTON1) {
            int x = e.getX(), y = e.getY();

            if (MODE_ACTUAL.equals(MODE_RECTANGLE) || MODE_ACTUAL.equals(MODE_ELLIPSE)) {
                points.clear();
                points.add(new Point(x, y));
                points.add(new Point(x, y));
                repaint();
            }
        }

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (actualMouse == MouseEvent.BUTTON1) {
            int x = e.getX(), y = e.getY();

            if (x >= 0 && y >= 0 && x < size.width && y < size.height) {
                if (MODE_ACTUAL.equals(MODE_RECTANGLE) || MODE_ACTUAL.equals(MODE_ELLIPSE)) {
                    points.get(1).x = x;
                    points.get(1).y = y;
                }

            }
        }

        repaint();
    }

    // -------------------------------------------------------------------
    @Override
    public void actionPerformed(ActionEvent e) {
        //repaint();
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}