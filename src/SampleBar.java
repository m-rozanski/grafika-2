import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

class SampleBar extends JPanel implements ActionListener {
    private Dimension size;
    private final int BORDER_SIZE = 1;

    private JList<Shape> list;
    private JScrollPane scrollPane;
    private DefaultListModel<Shape> listModel;
    private String name = "";

    private JButton btnAdd;
    private JButton btnDelete;
    private final String COMMAND_ADD = "Add";
    private final String COMMAND_DELETE = "Delete";
    private SampleBarClickListener listener;


    SampleBar() {
        super();
        this.setBorder(BorderFactory.createMatteBorder(0, BORDER_SIZE, 0, 0, Color.BLACK));
        initList();
        btnAdd = new JButton("ADD");
        btnDelete = new JButton("DELETE");
        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        size = getSize();
        int buttonWidth = size.width / 2;
        int buttonHeight = 35;
        btnAdd.setMinimumSize(new Dimension(buttonWidth, buttonHeight));
        btnAdd.setMaximumSize(new Dimension(buttonWidth, buttonHeight));
        btnAdd.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
        btnAdd.addActionListener(this);
        btnAdd.setActionCommand(COMMAND_ADD);
        btnDelete.setMinimumSize(new Dimension(buttonWidth, buttonHeight));
        btnDelete.setMaximumSize(new Dimension(buttonWidth, buttonHeight));
        btnDelete.setPreferredSize(new Dimension(buttonWidth, buttonHeight));
        btnDelete.addActionListener(this);
        btnDelete.setActionCommand(COMMAND_DELETE);

        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        this.add(scrollPane, c);

        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.weighty = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(btnAdd, c);

        c.gridx = 1;
        c.gridy = 1;
        c.gridwidth = 1;
        c.weighty = 0;
        c.weightx = 0.5;
        c.fill = GridBagConstraints.HORIZONTAL;
        this.add(btnDelete, c);
    }

    private void initList() {
        listModel = new DefaultListModel<>();

        list = new JList<>(listModel);
        list.setCellRenderer(new ImageRenderer());

        list.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

            }
        });

        scrollPane = new JScrollPane(list, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    }

    public void addItem(Shape shape) {
        listModel.addElement(shape);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int index = list.getSelectedIndex();

        if (e.getActionCommand().equals(COMMAND_ADD)) {
            listener.addClicked();
        } else if (e.getActionCommand().equals(COMMAND_DELETE)) {
            if(listModel.size() > 0) {
                listModel.remove(index);
                listener.deleteClicked(index);
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        size = getSize();
    }

    class ImageRenderer extends DefaultListCellRenderer {
        @Override
        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
            Shape sh = (Shape) value;
            BufferedImage image = sh.image;
            double scale = (double) 50 / (double) image.getHeight();
            ImageIcon imageIcon = scaleImage(image, scale);
            label.setText("");
            label.setIcon(imageIcon);
            label.setBorder(BorderFactory.createLineBorder(sh.color));
            return label;
        }
    }

    interface SampleBarClickListener {
        void addClicked();

        void deleteClicked(int index);
    }

    public void setListener(SampleBarClickListener Listener) {
        listener = Listener;
    }

    private ImageIcon scaleImage(BufferedImage image, double scale) {
        return new ImageIcon(image.getScaledInstance((int) (image.getWidth() * scale), (int) (image.getHeight() * scale), Image.SCALE_SMOOTH));
    }

    public ArrayList<Shape> getShapes() {
        ArrayList<Shape> shapes = new ArrayList<>();
        int size = listModel.getSize();
        for (int i = 0; i < size; i++) {
            shapes.add(listModel.getElementAt(i));
        }
        return shapes;
    }

    public void setName(String newName) {
        name = newName;
    }

    public String getName() {
        return name;
    }

    public void clear(){
        listModel.clear();
        name = "";
    }
}

