import org.xml.sax.SAXException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MainFrame extends JFrame implements ToolBar.ToolBarClickListener, SampleBar.SampleBarClickListener {
    private JPanel mainPanel;
    private ToolBar toolBar;
    private Workspace workspace;
    private SampleBar sampleBar;
    private int toolbarHeight;
    private int sampleBarWidth;

    MainFrame() {
        super();
        initUI();
    }

    private void initUI() {
        this.setTitle("Grafika 2");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        workspace = new Workspace();
        workspace.setMinimumSize(new Dimension(1000, 600));
        workspace.setMaximumSize(new Dimension(1000, 600));
        workspace.setPreferredSize(new Dimension(1000, 600));

        sampleBar = new SampleBar();
        sampleBar.setMinimumSize(new Dimension(200, 600));
        sampleBar.setMaximumSize(new Dimension(200, 600));
        sampleBar.setPreferredSize(new Dimension(200, 600));
        sampleBar.setListener(this);

        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        mainPanel.add(workspace, c);

        c.gridx = 1;
        c.gridheight = 1;
        c.weighty = 1;
        c.weightx = 0;
        c.fill = GridBagConstraints.VERTICAL;
        mainPanel.add(sampleBar, c);

        toolBar = new ToolBar();
        toolBar.setListener(this);

        this.getContentPane().add(toolBar, BorderLayout.NORTH);
        this.getContentPane().add(mainPanel, BorderLayout.CENTER);

        this.setSize(new Dimension(1280, 720));
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.pack();
        this.setVisible(true);
        toolbarHeight = this.getHeight() - workspace.getHeight();
        sampleBarWidth = this.getWidth() - workspace.getWidth();
    }

    @Override
    public void openClicked() {
        JFileChooser fileChooser = new JFileChooser();
        int result = fileChooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            BufferedImage image;
            File file = fileChooser.getSelectedFile();
            try {
                image = ImageIO.read(file);
                this.setSize(new Dimension(image.getWidth() + sampleBarWidth, image.getHeight() + toolbarHeight));
                workspace.clear();
                workspace.setSize(new Dimension(image.getWidth(), image.getHeight()));
                workspace.loadImage(image);
                sampleBar.clear();
                sampleBar.setName(file.getName().replaceFirst("[.][^.]+$", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadClicked() {
        if (workspace.isImageLoaded()) {
            XMLTool xmlTool = new XMLTool();
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    return f.isDirectory() || f.getName().toLowerCase().endsWith(".xml");
                }

                @Override
                public String getDescription() {
                    return null;
                }
            });
            int result = fileChooser.showOpenDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                try {
                    File file = fileChooser.getSelectedFile();
                    boolean read = false;
                    if(xmlTool.isCompatible(file, sampleBar.getName())){
                        read = true;
                    }
                    else{
                        int choice = JOptionPane.showConfirmDialog(null,"XML's image name does not match actual loaded image name. Continue anyway?","Warning", JOptionPane.YES_NO_OPTION);
                        if (choice == 0)
                            read = true;
                    }

                    if(read) {
                        ArrayList<Shape> shapes = xmlTool.read(file);
                        for (Shape shape : shapes) {
                            workspace.loadImageToShape(shape);
                            sampleBar.addItem(shape);
                            workspace.addShape(shape);
                        }
                    }
                } catch (ParserConfigurationException | IOException | SAXException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void saveClicked() {
        if (workspace.isImageLoaded()) {
            ArrayList<Shape> shapes = sampleBar.getShapes();
            XMLTool xmlTool = new XMLTool();
            try {
                xmlTool.initNew(sampleBar.getName());
                for (Shape shape : shapes) {
                    xmlTool.add(shape);
                }

                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);
                String path = "";
                if (chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                    path = chooser.getSelectedFile().getPath();
                    xmlTool.save(path);
                } else {
                }
            } catch (ParserConfigurationException | TransformerException pe) {
                pe.printStackTrace();
            }
        }
    }

    @Override
    public void rectangleClicked() {
        workspace.setModeRectangle();
    }

    @Override
    public void ellipseClicked() {
        workspace.setModeEllipse();
    }

    @Override
    public void polygonClicked() {
        workspace.setModePolygon();
    }

    @Override
    public void colorPickerClicked() {
        Color color = JColorChooser.showDialog(null, "Choose a color", Color.red);
        workspace.setColor(color);
    }

    @Override
    public void addClicked() {
        if (workspace.isImageLoaded()) {
            Shape shape = workspace.getShapeFromImage();
            workspace.addShape(shape);
            BufferedImage image = shape.image;
            if (image != null) {
                sampleBar.addItem(shape);
            }
        }
    }

    @Override
    public void deleteClicked(int index) {
        workspace.deleteShape(index);
    }
}
